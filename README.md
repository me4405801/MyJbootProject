
#打包部署流程
##打包
运行  mvn clean package

3.复制/target/[项目名]到部署目录，运行start.bat即可

管理员账号：admin密码666666

## 参考

####使用sharding-jdbc分表，方便后期扩展

#===============注意==============
#####layUI的DataTable即使设置选项page:false，最终渲染数据也会受到limit选项数量限制，默认10条

#####@Transactional必须在第一个调用的service方法上才有效，例如：
@Transactional  
public Ret A(){//公共方法   
    return B();   
}  
private Ret B(){  
    return Ret.ok();  
}
#####==========以上有效===========
public Ret A(){//公共方法   
    return B();  
}  
@Transactional  
private Ret B(){  
    return Ret.ok();  
}
#####==========以上无效===========

#####如果需要利用Set去重，比如做树形列表展示，JbootModel默认已经根据id覆写equals和hashcode，无需在Model中自定义覆写，除非有id之外的需求

#####由于sharding-jdbc有些sql语法不支持，而且官网没有细说，比如INSERT INTO XXX SELECT XXX 这种就不支持（报错空指针），因此如果不需要分库，只有分表时，最好利用jBoot的多数据源配置将分表的数据源和不分表的数据源分开配置
#####可以关注一下com.dage.salesflow.shardingjdbc.ShardingJdbcConfig，动态添加分表数据，应用场景：项目中的表类似于系统日志，是随着时间动态生成的，可能一开始并没有，类似于t_log202301,t_log202302,t_log202303，也就是说，无法一开始就在配置文件中写死actualDataNodes，这时可以使用ShardingJdbcConfig.addTableNodes()动态添加分表数据

#####如果项目分表规则非常简单，可以考虑不引入sharding-jdbc而手动实现


###自定义导入验证框架
#####可以在导入Excel、插入、更新、删除时自动进行验证，详见com.dage.salesflow.validation下的自定义注解
#####所有日期类型字段，导入导出时自动获取/设置 《属性名+Str》的属性，此行为会在代码生成器中自动完成
#####所有外键类型字段，导入自动获取 《属性名+Str》的属性进行查询，此行为会在代码生成器中自动完成
#####所有枚举类型字段，导入导出时自动获取/设置 《属性名+Name》的属性，此行为会在代码生成器中自动完成

###系统初始化导入或大数据量时可以在service中调整导入模式importMode

###前端封装js/common.js
#####myAjax：自定义ajax请求，自带登录状态验证
#####myTableRend：自定义DataTable渲染，自带登录状态验证formatDate:["属性名"]自动格式化日期为yyyy-MM-dd（默认yyyy-MM-dd HH:mm:ss）
#####myTreeTableRend：自定义TreeTable渲染，自带登录状态验证
#####initImport：自动初始化导入模块
#####getQueryString：获取url中的参数
#####json2param：json转url参数
#####verifyForm：自定义表单验证
#####layerForm：自定义弹出窗口渲染

###自定义代码生成器
#####自动生成Model、ExcelModel、Service、Controller、web页面
#####生成的文件位于/generate目录下
#####生成器程序位置：test/com.dage.Generator，并且可在com.dage.MyMetaBuilder定义数据表字段的约束信息，用于自动生成ExcelModel的注解以及Service中的插入、更新、删除校验，详见com.dage.ExcelColumnMeta
#####***注意***：生成的@ImportKey(columns = {"code"})注解需要手动修改

###tableSelect

    tableSelect.render({
         elem: '#user',//渲染元素id
         name:'userId',//表单提交时的name
         checkedKey: 'id',//数据id列名，默认id
         checkedText: 'name',//选择回显数据列名，默认name
         searchKey: 'name',//简单搜索字段名
         searchPlaceholder: '请输入用户名称',//输入框placeholder
         verify: 'required',//lay-verify属性
         // searchType: 'more', //开启多条件搜索
         // searchList: [
         //     {searchKey: 'name', searchPlaceholder: '搜索用户姓名'}, //搜索条件1
         //     {searchKey: 'dept', searchPlaceholder: '搜索部门名称'},  //搜索条件2
         //     {type:"hide",name:"status",value:1}//附加隐藏域，额外条件
         // ],
         width: 200,
         table: {
             url: '/user/listByPage',
             cols: [[
                 {type: 'radio'},//单选模式
                // {type: 'checkbox'},//多选模式
                 {field: 'name', title: '名称', align: "center"}
             ]],
             limits: [10, 15, 20, 25, 50, 100],
             limit: 10,
             page: true,
             skin: 'line'
         },
         done: function (elem, data) {
            //elem被渲染的dom对象
            //data选中的数据
             if (data.data.length > 0) {
                 elem.val(data.data[0].firstName+data.data[0].lastName);
             }
         }
     });

[jFinal](https://jfinal.com/)  
[jBoot](http://www.jboot.io)  
[shardingjdbc](https://shardingsphere.apache.org/document/4.1.1/en/manual/sharding-jdbc/configuration/config-yaml/)  
[ES](https://www.elastic.co/guide/en/elasticsearch/client/java-api-client/7.17/installation.html)  
[layuimini](https://gitee.com/zhongshaofa/layuimini)  
[layui](https://layui.gitee.io/v2/)  
[Jfinal+Undertow自定义对静态资源的404错误处理](https://jfinal.com/share/1886)  
[sa-token](https://sa-token.cc)  
[mui](https://dev.dcloud.net.cn/mui/ui/)  
[treetable](https://blog.csdn.net/qq_40205116/article/details/89672232)  
[xm-select](https://maplemei.gitee.io/xm-select/#/component/install)  
[图片放大组件](https://jfinal.com/share/2614)  
[echarts](https://echarts.apache.org/zh/index.html)  
[EasyExcel](https://easyexcel.opensource.alibaba.com/)  
[阿里导入导出](https://github.com/alibaba/AGEIPort)  