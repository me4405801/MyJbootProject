/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50557
Source Host           : localhost:3306
Source Database       : sales_flow

Target Server Type    : MYSQL
Target Server Version : 50557
File Encoding         : 65001

Date: 2023-03-03 16:46:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for product_profile
-- ----------------------------
DROP TABLE IF EXISTS `product_profile`;
CREATE TABLE `product_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL COMMENT '产品编码',
  `name` varchar(100) DEFAULT NULL COMMENT '产品名称',
  `unit` int(11) DEFAULT NULL COMMENT '产品单位（t_dict:product_unit）',
  `spec` int(11) DEFAULT NULL COMMENT '产品规格（t_dict:product_spec）',
  `type` int(11) DEFAULT NULL COMMENT '产品类别（t_dict:product_type）',
  `capacity` smallint(6) DEFAULT NULL COMMENT '件装量',
  `expiry_date` smallint(6) DEFAULT NULL COMMENT '有效期（月）',
  `is_medical` tinyint(4) DEFAULT '0' COMMENT '是否医保0.否1.是',
  `is_basic_drug` tinyint(4) DEFAULT '0' COMMENT '是否基药0.否1.是',
  `create_user` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` int(11) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `area_id` int(11) DEFAULT NULL COMMENT '区域id(t_area.id)',
  `is_deleted` tinyint(4) DEFAULT '0' COMMENT '是否删除：0.否1.是',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `codeIdx` (`code`),
  KEY `expiryDateIndex` (`expiry_date`) USING BTREE,
  KEY `isMedicalIndex` (`is_medical`) USING BTREE,
  KEY `isBasicDrugIndex` (`is_basic_drug`) USING BTREE,
  KEY `isDelIdx` (`is_deleted`) USING BTREE,
  KEY `nameIdx` (`name`),
  KEY `aidIdx` (`area_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='产品档案';

-- ----------------------------
-- Records of product_profile
-- ----------------------------
INSERT INTO `product_profile` VALUES ('1', 'CP001', '测试产品1', '72', '82', '80', '222', null, '0', '0', '1', '2023-02-13 13:52:42', '1', '2023-02-13 16:22:39', null, '0');
INSERT INTO `product_profile` VALUES ('2', 'CP002', '测试产品2', '72', '83', '80', '100', null, '1', '1', '1', '2023-02-13 14:35:30', null, null, '3', '0');

-- ----------------------------
-- Table structure for t_area
-- ----------------------------
DROP TABLE IF EXISTS `t_area`;
CREATE TABLE `t_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0' COMMENT '上级区域id',
  `name` varchar(255) DEFAULT NULL COMMENT '区域名称',
  `code` varchar(255) DEFAULT NULL COMMENT '区域编码',
  `sort` int(11) DEFAULT NULL COMMENT '排序序号',
  `note` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `codeIdx` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='区域表';

-- ----------------------------
-- Records of t_area
-- ----------------------------
INSERT INTO `t_area` VALUES ('1', '0', '全国', '00001', '0', null);
INSERT INTO `t_area` VALUES ('2', '1', '华东大区', '0000100002', '0', null);
INSERT INTO `t_area` VALUES ('3', '2', '安徽', '000010000200003', '0', 't1');
INSERT INTO `t_area` VALUES ('4', '1', '华北大区', '0000100004', '1', null);

-- ----------------------------
-- Table structure for t_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_dict`;
CREATE TABLE `t_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0' COMMENT '父级id',
  `dic_key` varchar(255) DEFAULT NULL,
  `dic_value` varchar(255) DEFAULT NULL,
  `dict_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统字典';

-- ----------------------------
-- Records of t_dict
-- ----------------------------
INSERT INTO `t_dict` VALUES ('1', '0', 'position_list', '职位', '0');
INSERT INTO `t_dict` VALUES ('2', '1', 'position', '大区总监', '0');
INSERT INTO `t_dict` VALUES ('3', '1', 'position', '业务员', '1');
INSERT INTO `t_dict` VALUES ('4', '0', 'target_customer_type_list', '目标客户类型', null);
INSERT INTO `t_dict` VALUES ('5', '4', 'target_customer_type', '连锁门店', null);
INSERT INTO `t_dict` VALUES ('6', '4', 'target_customer_type', '医务室', null);
INSERT INTO `t_dict` VALUES ('7', '4', 'target_customer_type', '目标连锁', null);
INSERT INTO `t_dict` VALUES ('8', '4', 'target_customer_type', '社区医院', null);
INSERT INTO `t_dict` VALUES ('9', '4', 'target_customer_type', '诊所', null);
INSERT INTO `t_dict` VALUES ('10', '4', 'target_customer_type', '经销商', null);
INSERT INTO `t_dict` VALUES ('11', '4', 'target_customer_type', '分销商', null);
INSERT INTO `t_dict` VALUES ('12', '4', 'target_customer_type', '门诊', null);
INSERT INTO `t_dict` VALUES ('13', '4', 'target_customer_type', '医院', null);
INSERT INTO `t_dict` VALUES ('14', '4', 'target_customer_type', '保健站', null);
INSERT INTO `t_dict` VALUES ('15', '4', 'target_customer_type', '单体店', null);
INSERT INTO `t_dict` VALUES ('16', '4', 'target_customer_type', '卫生院', null);
INSERT INTO `t_dict` VALUES ('17', '4', 'target_customer_type', '三级商业', null);
INSERT INTO `t_dict` VALUES ('18', '4', 'target_customer_type', '四级商业', null);
INSERT INTO `t_dict` VALUES ('19', '4', 'target_customer_type', '疗养院', null);
INSERT INTO `t_dict` VALUES ('20', '4', 'target_customer_type', '其他', null);
INSERT INTO `t_dict` VALUES ('35', '0', 'dealer_level_list', '经销商级别', null);
INSERT INTO `t_dict` VALUES ('36', '35', 'dealer_level', '经销商', null);
INSERT INTO `t_dict` VALUES ('37', '35', 'dealer_level', '分销商', null);
INSERT INTO `t_dict` VALUES ('38', '35', 'dealer_level', '三级商业', null);
INSERT INTO `t_dict` VALUES ('39', '35', 'dealer_level', '四级商业', null);
INSERT INTO `t_dict` VALUES ('42', '0', 'customer_pattern_list', '商业模式', null);
INSERT INTO `t_dict` VALUES ('43', '42', 'customer_pattern', '单体', null);
INSERT INTO `t_dict` VALUES ('44', '42', 'customer_pattern', '直营/加盟', null);
INSERT INTO `t_dict` VALUES ('45', '42', 'customer_pattern', '商业', null);
INSERT INTO `t_dict` VALUES ('46', '42', 'customer_pattern', '全国/区域', null);
INSERT INTO `t_dict` VALUES ('47', '42', 'customer_pattern', '医院', null);
INSERT INTO `t_dict` VALUES ('49', '0', 'customer_category_list', '客户类别', null);
INSERT INTO `t_dict` VALUES ('50', '49', 'customer_category', '零售', '0');
INSERT INTO `t_dict` VALUES ('51', '49', 'customer_category', '商业', '1');
INSERT INTO `t_dict` VALUES ('52', '49', 'customer_category', '医院', '2');
INSERT INTO `t_dict` VALUES ('53', '49', 'customer_category', '第三终端', '3');
INSERT INTO `t_dict` VALUES ('54', '51', 'customer_type', '经销商', '0');
INSERT INTO `t_dict` VALUES ('55', '51', 'customer_type', '分销商', '1');
INSERT INTO `t_dict` VALUES ('56', '51', 'customer_type', '三级商业', '2');
INSERT INTO `t_dict` VALUES ('57', '51', 'customer_type', '四级商业', '3');
INSERT INTO `t_dict` VALUES ('58', '50', 'customer_type', '连锁门店', '0');
INSERT INTO `t_dict` VALUES ('59', '50', 'customer_type', '目标连锁', '1');
INSERT INTO `t_dict` VALUES ('60', '50', 'customer_type', '单体店', '2');
INSERT INTO `t_dict` VALUES ('61', '50', 'customer_type', '卖厂', '3');
INSERT INTO `t_dict` VALUES ('62', '52', 'customer_type', '医院', '0');
INSERT INTO `t_dict` VALUES ('63', '52', 'customer_type', '社区医院', '1');
INSERT INTO `t_dict` VALUES ('64', '53', 'customer_type', '医务室', '0');
INSERT INTO `t_dict` VALUES ('65', '53', 'customer_type', '卫生院', '1');
INSERT INTO `t_dict` VALUES ('66', '53', 'customer_type', '门诊部', '2');
INSERT INTO `t_dict` VALUES ('67', '53', 'customer_type', '诊所', '3');
INSERT INTO `t_dict` VALUES ('68', '53', 'customer_type', '保健站', '4');
INSERT INTO `t_dict` VALUES ('69', '53', 'customer_type', '疗养院', '5');
INSERT INTO `t_dict` VALUES ('70', '53', 'customer_type', '其他', '6');
INSERT INTO `t_dict` VALUES ('71', '0', 'product_unit_list', '产品单位', null);
INSERT INTO `t_dict` VALUES ('72', '71', 'product_unit', '箱', '0');
INSERT INTO `t_dict` VALUES ('73', '71', 'product_unit', '袋', '1');
INSERT INTO `t_dict` VALUES ('74', '71', 'product_unit', '盒', '2');
INSERT INTO `t_dict` VALUES ('75', '71', 'product_unit', '件', '3');
INSERT INTO `t_dict` VALUES ('76', '71', 'product_unit', '瓶', '4');
INSERT INTO `t_dict` VALUES ('77', '71', 'product_unit', '支', '5');
INSERT INTO `t_dict` VALUES ('78', '0', 'product_spec_list', '产品规格', null);
INSERT INTO `t_dict` VALUES ('79', '0', 'product_type_list', '产品类别', null);
INSERT INTO `t_dict` VALUES ('80', '79', 'product_type', '商销产品', '0');
INSERT INTO `t_dict` VALUES ('81', '79', 'product_type', '零售产品', '1');
INSERT INTO `t_dict` VALUES ('82', '78', 'product_spec', '测试规格1', '0');
INSERT INTO `t_dict` VALUES ('83', '78', 'product_spec', '测试规格2', '1');

-- ----------------------------
-- Table structure for t_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_resource`;
CREATE TABLE `t_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型：0.菜单1.权限2.首页',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父菜单id',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标class',
  `url` varchar(255) DEFAULT NULL COMMENT '菜单url',
  `menu_order` int(11) DEFAULT NULL COMMENT '排序序号',
  `code` varchar(255) DEFAULT NULL COMMENT '权限code',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of t_resource
-- ----------------------------
INSERT INTO `t_resource` VALUES ('1', '销售流向', '0', '0', 'fa fa-address-book', null, '0', null, null);
INSERT INTO `t_resource` VALUES ('2', '系统管理', '0', '0', 'layui-icon layui-icon-set', null, '999', '', null);
INSERT INTO `t_resource` VALUES ('3', '用户管理', '0', '2', 'layui-icon layui-icon-username', '/admin/user-list.html', '10', '', null);
INSERT INTO `t_resource` VALUES ('4', '菜单权限', '0', '2', 'layui-icon layui-icon-list', '/admin/menu-list.html', '20', '', null);
INSERT INTO `t_resource` VALUES ('5', '角色管理', '0', '2', 'layui-icon layui-icon-user', '/admin/role-list.html', '30', '', null);
INSERT INTO `t_resource` VALUES ('6', '添加用户', '1', '3', null, null, '10', 'user:add', null);
INSERT INTO `t_resource` VALUES ('7', '修改用户', '1', '3', '', '', '20', 'user:update', '');
INSERT INTO `t_resource` VALUES ('8', '首页', '2', '0', '', '/home.html', null, '', '');
INSERT INTO `t_resource` VALUES ('9', '字典管理', '0', '2', 'layui-icon layui-icon-read', '/admin/dict-list.html', '40', null, null);
INSERT INTO `t_resource` VALUES ('21', '行政区域', '0', '2', 'layui-icon layui-icon-location', '/admin/area-list.html', '50', null, null);
INSERT INTO `t_resource` VALUES ('22', '基础档案', '0', '1', 'layui-icon layui-icon-file', null, '0', null, null);
INSERT INTO `t_resource` VALUES ('24', '产品档案', '0', '22', 'layui-icon layui-icon-component', '/page/ProductProfile-list.html', '1', null, null);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL COMMENT '角色名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '管理员', null);
INSERT INTO `t_role` VALUES ('2', '普通用户', null);
INSERT INTO `t_role` VALUES ('3', '业务管理员', null);
INSERT INTO `t_role` VALUES ('4', '部门领导', null);

-- ----------------------------
-- Table structure for t_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_role_resource`;
CREATE TABLE `t_role_resource` (
  `role_id` int(11) DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色权限';

-- ----------------------------
-- Records of t_role_resource
-- ----------------------------
INSERT INTO `t_role_resource` VALUES ('1', '8');
INSERT INTO `t_role_resource` VALUES ('1', '6');
INSERT INTO `t_role_resource` VALUES ('1', '7');
INSERT INTO `t_role_resource` VALUES ('3', '1');
INSERT INTO `t_role_resource` VALUES ('3', '22');
INSERT INTO `t_role_resource` VALUES ('3', '2');
INSERT INTO `t_role_resource` VALUES ('3', '3');
INSERT INTO `t_role_resource` VALUES ('3', '21');
INSERT INTO `t_role_resource` VALUES ('4', '1');
INSERT INTO `t_role_resource` VALUES ('4', '22');
INSERT INTO `t_role_resource` VALUES ('2', '8');
INSERT INTO `t_role_resource` VALUES ('3', '8');
INSERT INTO `t_role_resource` VALUES ('4', '8');
INSERT INTO `t_role_resource` VALUES ('2', '1');
INSERT INTO `t_role_resource` VALUES ('2', '22');
INSERT INTO `t_role_resource` VALUES ('2', '24');
INSERT INTO `t_role_resource` VALUES ('1', '1');
INSERT INTO `t_role_resource` VALUES ('1', '22');
INSERT INTO `t_role_resource` VALUES ('1', '24');
INSERT INTO `t_role_resource` VALUES ('1', '2');
INSERT INTO `t_role_resource` VALUES ('1', '3');
INSERT INTO `t_role_resource` VALUES ('1', '4');
INSERT INTO `t_role_resource` VALUES ('1', '5');
INSERT INTO `t_role_resource` VALUES ('1', '9');
INSERT INTO `t_role_resource` VALUES ('1', '21');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `name` varchar(255) DEFAULT NULL COMMENT '用户姓名',
  `role_id` int(11) NOT NULL DEFAULT '1' COMMENT '角色（岗位）',
  `area_id` int(11) NOT NULL COMMENT '区域',
  `code` varchar(100) NOT NULL COMMENT '员工编号',
  `sex` tinyint(2) DEFAULT '1' COMMENT '性别：1.男0.女',
  `weixin` varchar(255) DEFAULT NULL COMMENT '微信',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) DEFAULT NULL COMMENT '电子邮件',
  `qq` varchar(255) DEFAULT NULL COMMENT 'QQ',
  `id_number` varchar(100) DEFAULT NULL COMMENT '身份证',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '账户状态：0.封停1.正常',
  `entry_time` datetime DEFAULT NULL COMMENT '入职时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_user` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` int(11) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `codeIdx` (`code`) USING BTREE,
  KEY `aidIdx` (`area_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', 'dfi4i6vPRnfi9BeTBwExIA==', '系统管理员', '1', '1', 'SYS001', '1', null, null, null, null, '340102199912124333', '1', '2023-02-01 00:00:00', null, null, null, '1', '2023-02-23 14:13:40');
INSERT INTO `t_user` VALUES ('2', 'test', 'dfi4i6vPRnfi9BeTBwExIA==', null, '2', '2', 'AK001', '1', null, null, null, null, null, '1', '2022-06-01 00:00:00', null, null, null, '1', '2023-02-03 15:22:37');
INSERT INTO `t_user` VALUES ('4', '云天河', 'dfi4i6vPRnfi9BeTBwExIA==', '云天河', '3', '3', 'AK002', '1', null, null, null, null, '340102199912124333', '1', '2023-02-01 00:00:00', null, '1', '2023-02-03 15:33:43', null, null);
INSERT INTO `t_user` VALUES ('5', '柳梦璃', null, '柳梦璃', '4', '4', 'AK003', '0', null, null, null, null, '340102199912124444', '1', '2023-12-03 00:00:00', null, '1', '2023-02-03 15:44:50', '1', '2023-02-03 16:13:55');
INSERT INTO `t_user` VALUES ('6', '韩菱纱', 'AQIrbTn2HW100WtaJPHmHw==', '韩菱纱', '4', '4', 'AK004', '0', null, null, null, null, '340102199912124444', '1', '2023-12-03 00:00:00', null, '1', '2023-02-03 16:10:52', null, null);
