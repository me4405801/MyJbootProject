package com.dage;

import com.jfinal.kit.PathKit;
import io.jboot.app.JbootApplication;
import io.jboot.codegen.service.JbootServiceImplGenerator;

public class Generator {
	public static final String GENERATE_DIR = "/generated/";

	public static void main(String[] args) {
		// 配置数据库的数据源
		JbootApplication.setBootArg("jboot.datasource.url", "jdbc:mysql://127.0.0.1:3306/sales_flow?characterEncoding=utf8&characterSetResults=utf8&autoReconnect=true&failOverReadOnly=false&useInformationSchema=true");
		JbootApplication.setBootArg("jboot.datasource.user", "root");
		JbootApplication.setBootArg("jboot.datasource.password", "root");

		String projectPackage = "com.dage.salesflow";
		String modelPackage = "model";
		String baseModelPackage = modelPackage + ".base";

		String modelDir = PathKit.getWebRootPath() + GENERATE_DIR + modelPackage.replace(".", "/");
		String baseModelDir = PathKit.getWebRootPath() + GENERATE_DIR + baseModelPackage.replace(".", "/");
		System.out.println("start generate...");
		System.out.println("generate dir:" + modelDir);
		// 生成 Model
		//		new JbootBaseModelGenerator(baseModelPackage, baseModelDir).generate(metaBuilder.build());
		//使用自定义BaseModelGenerator
		new MyBaseModelGenerator(projectPackage, baseModelPackage, baseModelDir, true).generate();
		new MyModelGenerator(projectPackage, projectPackage + "." + baseModelPackage, modelDir).setRemovedTableNamePrefixes("t_").generate();

		//		generatService(modelPackage);

		generateMyService(projectPackage);

		generateMyController(projectPackage);

		generateExcelModel(projectPackage);

		generateWebPage(projectPackage);
	}

	private static void generateWebPage(String projectPackage) {
		String webPackage = "page";

		String webOutputDir = PathKit.getWebRootPath() + GENERATE_DIR + webPackage.replace(".", "/");

		// 生成 html
		new MyWebGenerator(projectPackage, webOutputDir).generate();
	}

	private static void generateExcelModel(String projectPackage) {
		String excelPackage = "excel";

		String excelOutputDir = PathKit.getWebRootPath() + GENERATE_DIR + excelPackage.replace(".", "/");
		// 生成
		new MyExcelModelGenerator(projectPackage, excelOutputDir).generate();
	}

	private static void generateMyService(String projectPackage) {
		String servicePackage = "service";

		String serviceImplOutputDir = PathKit.getWebRootPath() + GENERATE_DIR + servicePackage.replace(".", "/");

		// 生成 Service
		new MyServiceGenerator(projectPackage, serviceImplOutputDir).setRemovedTableNamePrefixes("t_").generate();
	}

	private static void generateMyController(String projectPackage) {
		String controllerPackage = "controller";

		String controllerOutputDir = PathKit.getWebRootPath() + GENERATE_DIR + controllerPackage.replace(".", "/");

		// 生成 Controller
		new MyControllerGenerator(projectPackage, controllerOutputDir).setRemovedTableNamePrefixes("t_").generate();
	}

	private static void generateService(String modelPackage) {
		String servicePackage = "com.dage.salesflow.generate.service";
		String serviceImplPackage = "com.dage.salesflow.generate.service.impl";

		//		String serviceOutputDir = PathKit.getWebRootPath() + "/src/main/java/" + servicePackage.replace(".", "/");
		String serviceImplOutputDir = PathKit.getWebRootPath() + "/src/main/java/" + serviceImplPackage.replace(".", "/");

		// 生成 Service 接口 及其 实现类
		//		new JbootServiceInterfaceGenerator(servicePackage, serviceOutputDir, modelPackage).generate();
		new JbootServiceImplGenerator(servicePackage, serviceImplPackage, serviceImplOutputDir, modelPackage).setRemovedTableNamePrefixes("t_").setImplName("impl").generate();
	}
}
