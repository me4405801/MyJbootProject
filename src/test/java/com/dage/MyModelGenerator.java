package com.dage;

import io.jboot.codegen.model.JbootModelGenerator;

public class MyModelGenerator extends JbootModelGenerator {
	public MyModelGenerator(String modelPackageName, String baseModelPackageName, String modelOutputDir) {
		super(modelPackageName, baseModelPackageName, modelOutputDir);
		this.template = "model_template.tp";
	}
}
