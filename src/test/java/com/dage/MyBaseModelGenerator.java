package com.dage;

import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import io.jboot.codegen.CodeGenHelpler;
import io.jboot.codegen.model.JbootBaseModelGenerator;

public class MyBaseModelGenerator extends JbootBaseModelGenerator {

	private MyMetaBuilder metaBuilder;

	public MyBaseModelGenerator(String projectPackage, String baseModelPackageName, String baseModelOutputDir) {
		this(projectPackage, baseModelPackageName, baseModelOutputDir, false);
	}

	public MyBaseModelGenerator(String projectPackage, String baseModelPackageName, String baseModelOutputDir, boolean extendsBase) {
		super(baseModelPackageName, baseModelOutputDir);
		MyMetaBuilder metaBuilder = new MyMetaBuilder(CodeGenHelpler.getDatasource());
		metaBuilder.setDialect(new MysqlDialect());
		metaBuilder.setRemovedTableNamePrefixes("t_");
		metaBuilder.setGenerateRemarks(true);

		this.metaBuilder = metaBuilder;
		if (extendsBase) {
			this.template = "base_model_template.tp";
		}
	}

	public void generate() {
		generate(metaBuilder.build());
	}
}
