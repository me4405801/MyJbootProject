package com.dage;

import com.jfinal.plugin.activerecord.generator.ColumnMeta;

public class ExcelColumnMeta extends ColumnMeta {
	/**
	 * 是否唯一约束字段
	 */
	public boolean uniqueKey;
	/**
	 * 是否非空
	 */
	public boolean notNull;
	/**
	 * 是否应用数据权限，默认 是
	 */
	public boolean withDataScope = true;
	/**
	 * 是否逻辑删除，默认 是
	 */
	public boolean logicDelete = true;
	/**
	 * 是否外键约束字段
	 */
	public boolean foreignKey;
	/**
	 * 外键约束字段是否非空
	 */
	public boolean foreignKeyNotNull = true;
	/**
	 * 导入时用于查询id的关联数据表名称
	 */
	public String foreignTable;
	/**
	 * 导入时用于查询id的关联数据字段名称
	 */
	public String foreignColumn;
	/**
	 * 是否字典字段
	 */
	public boolean dictKey;
	/**
	 * 关联字典表的dict_key
	 */
	public String dictKeyType;
	/**
	 * 字典字段是否非空
	 */
	public boolean dictKeyNotNull = true;
	/**
	 * 是否正则验证字段
	 */
	public boolean regexValidate;
	/**
	 * 验证规则名称，详见ValidationInfo
	 */
	public String regexPattern;
	/**
	 * 是否枚举字段
	 */
	public boolean enumProp;
	/**
	 * “是否”类型属性，默认不是（false），如果设置为true，则忽略props设置，自动取SysConstant.YES_NO_PROP
	 */
	public boolean yesNo;
	/**
	 * 枚举数组
	 */
	public String[] props;
	/**
	 * 是否@ExcelIgnore
	 */
	public boolean excelIgnore;
	/**
	 * 是否日期类型
	 */
	public boolean dateStr;

	public boolean foreignKeyStr = false;
	public boolean dictKeyStr = false;
	public boolean enumPropName = false;
}
