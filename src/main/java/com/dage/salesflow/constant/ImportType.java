package com.dage.salesflow.constant;

/**
 * 导入类型
 */
public enum ImportType {
	FLOW, INVENTORY;

	@Override
	public String toString() {
		return String.valueOf(ordinal());
	}
}
