package com.dage.salesflow.constant;

/**
 * 导入Excel的模式
 */
public enum ImportMode {
	STRICT(0, "严格模式"),//任何警告或错误直接导入失败
	NORMAL(1, "正常模式"),//发生错误导入失败，警告仅提示
	SOFT(2, "宽松模式");//任何警告或错误不会导入失败

	final int value;
	final String name;

	ImportMode(int value, String name) {
		this.value = value;
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

}
