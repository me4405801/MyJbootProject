package com.dage.salesflow.constant;

public enum YesNoEnum {
	NO(0, "否"),
	YES(1, "是");
	final int value;
	final String name;

	YesNoEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public static YesNoEnum fromName(String name) {
		if ("是".equals(name)) {
			return YesNoEnum.YES;
		} else {
			return YesNoEnum.NO;
		}
	}

	public static YesNoEnum fromValue(int value) {
		if (value == 1) {
			return YesNoEnum.YES;
		} else {
			return YesNoEnum.NO;
		}
	}
}
