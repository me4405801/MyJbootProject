package com.dage.salesflow.constant;

public enum ImportStatus {
	PROCEEDING, FINISH, ERROR;

	@Override
	public String toString() {
		return String.valueOf(ordinal());
	}
}
