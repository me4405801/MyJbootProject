package com.dage.salesflow.constant;

import java.util.HashSet;
import java.util.Set;

public class SysConstant {
	public static final String PROJECT_NAME = "销售流向";

	public static final String[] YES_NO_PROP = {"否", "是"};

	public static final String DICT_TABLE = "t_dict";
	public static final String DICT_KEY = "dic_key";
	public static final String DICT_VALUE = "dic_value";

	//会话超时时间（秒）
	public static final int SESSION_TIMEOUT = 60 * 30;

	public static final int NOT_TOKEN = -21;
	public static final int INVALID_TOKEN = -22;
	public static final int TOKEN_TIMEOUT = -23;
	public static final int BE_REPLACED = -24;
	public static final int KICK_OUT = -25;

	public static final int USER_NOT_EXIST = -32;
	public static final int UNKNOWN_PASSWORD = -31;
	public static final int USER_LOCKED = -30;

	public static final int CONTENT_ILLEGAL = -90;
	public static final int NO_LOGIN = -20;
	public static final int ERR_CODE = -1;
	public static final int SUCCESS_CODE = 1;

	public static final int IMPORT_ERROR = 500;
	public static final int IMPORT_WARN = 501;

	//默认分页大小
	public static final int DEFAULT_PAGESIZE = 6;
	//默认第一页数据量大小
	public static final int DEFAULT_FIRST_PAGESIZE = DEFAULT_PAGESIZE * 2;
	//Cacheable默认缓存时间（秒）
	public static final int CACHE_EXPIRE = 30 * 24 * 3600;
	public static final int CAPTCHA_ERR = -4;
	public static final String KEY_CAPTCHA = "KEY_CAPTCHA";
	//手机验证码时间（重发间隔）（秒）
	public static final int PHONE_CAPTCHA_RETRY = 120;
	//手机验证码缓存时间（秒）
	public static final int PHONE_CAPTCHA_EXPIRE = 600;
	//注册手机验证码缓存key前缀
	public static final String REG_CAPTCHA_KEY_PREFIX = "REG_CAPTCHA:";
	//忘记密码手机验证码缓存key前缀
	public static final String PWD_CAPTCHA_KEY_PREFIX = "PWD_CAPTCHA:";
	public static final String BIND_PHONE_CAPTCHA_KEY_PREFIX = "BIND_PHONE_CAPTCHA:";

	public static final String PASS_WORD_KEY = "SalesFlow";

	public static final String CACHE_USER = "User";
	public static final String CACHE_AREA = "Area";
	public static final String CACHE_PRODUCT = "Product";
	public static final String CACHE_IMPORT_PROGRESS = "ImportProgress";
	public static final int CACHE_IMPORT_PROGRESS_TIMEOUT = 1800;//导入进度信息缓存时间（秒）
	public static final Set<String> CACHES = new HashSet<>();

	static {
		CACHES.add(CACHE_USER);
		CACHES.add(CACHE_AREA);
		CACHES.add(CACHE_PRODUCT);
		//		CACHES.add(CACHE_IMPORT_PROGRESS);进度信息不要清除
	}
}
