package com.dage.salesflow.kit;

import cn.dev33.satoken.stp.StpInterface;
import com.dage.salesflow.model.User;
import com.dage.salesflow.service.ResourceService;
import com.dage.salesflow.service.UserService;
import com.jfinal.aop.Aop;

import java.util.ArrayList;
import java.util.List;

public class AuthenticationService implements StpInterface {
	private UserService userService;
	private ResourceService resourceService;

	public AuthenticationService() {
		this.userService = Aop.get(UserService.class);
		this.resourceService = Aop.get(ResourceService.class);
	}

	@Override
	public List<String> getPermissionList(Object loginId, String loginType) {
		User user = userService.getById(Integer.parseInt(loginId.toString()));
		return resourceService.listResourceByRole(user.getRoleId());
	}

	@Override
	public List<String> getRoleList(Object loginId, String loginType) {
		User user = userService.getById(Integer.parseInt(loginId.toString()));
		List<String> roles = new ArrayList<>();
		roles.add("role" + user.getRoleId());
		return roles;
	}

}
