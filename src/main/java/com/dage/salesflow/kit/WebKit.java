package com.dage.salesflow.kit;

import javax.servlet.http.HttpServletRequest;

public class WebKit {
	public static boolean isAjaxRequest(HttpServletRequest request) {
		String requestedWith = request.getHeader("x-requested-with");
		return "XMLHttpRequest".equalsIgnoreCase(requestedWith);
	}

	public static long ipToInt(String ipStr) {
		String[] ip = ipStr.split("\\.");
		return (Integer.parseInt(ip[0]) << 24) + (Integer.parseInt(ip[1]) << 16) + (Integer.parseInt(ip[2]) << 8) + Integer.parseInt(ip[3]);
	}

	public static String intToIp(int intIp) {
		StringBuilder builder = new StringBuilder();
		builder.append((intIp >> 24) + ".");
		builder.append(((intIp & 0x00FFFFFF) >> 16) + ".");
		builder.append(((intIp & 0x0000FFFF) >> 8) + ".");
		builder.append((intIp & 0x000000FF));
		return builder.toString();
	}

}
