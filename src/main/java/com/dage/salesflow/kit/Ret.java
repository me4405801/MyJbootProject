package com.dage.salesflow.kit;

import com.alibaba.fastjson.annotation.JSONField;
import com.dage.salesflow.constant.SysConstant;
import io.jboot.web.json.JsonIgnore;

import java.io.Serializable;

public class Ret implements Serializable {
	private int code;
	private String msg;
	private Object data;

	@JsonIgnore
	@JSONField(serialize = false)
	public boolean isOk() {
		return code == SysConstant.SUCCESS_CODE;
	}

	public Ret() {
	}

	public Ret(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public Ret(int code, String msg, Object data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public static Ret ok() {
		return ok(null);
	}

	public static Ret ok(Object data) {
		return new Ret(SysConstant.SUCCESS_CODE, null, data);
	}

	public static Ret err(String msg) {
		return new Ret(SysConstant.ERR_CODE, msg);
	}

	public static Ret err(int code) {
		return new Ret(code, null);
	}

	public static Ret err(int code, Object data) {
		return new Ret(code, null, data);
	}

	public static Ret noLogin() {
		return new Ret(SysConstant.NO_LOGIN, "对不起，请先登录！");
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
