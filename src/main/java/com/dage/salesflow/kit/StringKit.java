package com.dage.salesflow.kit;

import java.util.UUID;

public class StringKit {
	/**
	 * 驼峰命名法转下划线
	 */
	public static String camelCase2Underline(String str) {
		int len = str.length();
		if (len <= 1) {
			return str;
		}
		StringBuilder sb = new StringBuilder();
		char ch;
		for (int i = 0; i < len; i++) {
			ch = str.charAt(i);
			if (Character.isUpperCase(ch)) {
				//大写字母，先添加下划线，在转小写
				sb.append('_');
				sb.append(Character.toLowerCase(ch));
			} else {
				sb.append(ch);
			}
		}
		return sb.toString();
	}

	public static String uuid() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
