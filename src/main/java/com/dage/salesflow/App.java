package com.dage.salesflow;

import cn.dev33.satoken.SaManager;
import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.context.SaTokenContextForThreadLocal;
import com.dage.salesflow.constant.SysConstant;
import com.dage.salesflow.es.ElasticSearchPlugin;
import com.dage.salesflow.interceptor.AuthInterceptor;
import com.dage.salesflow.interceptor.ExceptionInterceptor;
import com.dage.salesflow.json.MyJsonFactory;
import com.dage.salesflow.kit.AuthenticationService;
import com.jfinal.config.Constants;
import com.jfinal.config.Interceptors;
import io.jboot.aop.jfinal.JfinalPlugins;
import io.jboot.app.JbootApplication;
import io.jboot.core.listener.JbootAppListenerBase;

public class App extends JbootAppListenerBase {

	@Override
	public void onStart() {
		SaTokenConfig saTokenConfig = new SaTokenConfig();
		saTokenConfig.setActivityTimeout(SysConstant.SESSION_TIMEOUT);
		SaManager.setConfig(saTokenConfig);
		SaManager.setSaTokenContext(new SaTokenContextForThreadLocal());
		SaManager.setStpInterface(new AuthenticationService());
	}

	@Override
	public void onConstantConfig(Constants constants) {
		constants.setError404View("/err/404.html");
		constants.setError401View("/err/401.html");
		constants.setError500View("/err/500.html");
		//自定义json转换
		constants.setJsonFactory(MyJsonFactory::new);
	}

	@Override
	public void onPluginConfig(JfinalPlugins plugins) {
		plugins.add(new ElasticSearchPlugin("localhost", 9200, "elasticsearch"));
		super.onPluginConfig(plugins);
	}

	@Override
	public void onInterceptorConfig(Interceptors interceptors) {
		interceptors.add(new ExceptionInterceptor());
		interceptors.add(new AuthInterceptor());
	}

	public static void main(String[] args) {
		JbootApplication.run(args, webBuilder -> {
			webBuilder.add404ErrorPage("/err/404.html");
		});
	}
}
