package com.dage.salesflow.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.excel.util.ListUtils;
import com.dage.salesflow.constant.SysConstant;
import com.dage.salesflow.excel.ImportProgress;
import com.dage.salesflow.interceptor.AuthInterceptor;
import com.dage.salesflow.kit.Ret;
import com.dage.salesflow.model.Resource;
import com.dage.salesflow.model.User;
import com.dage.salesflow.model.vo.Menu;
import com.dage.salesflow.service.ResourceService;
import com.dage.salesflow.service.UserService;
import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import io.jboot.app.PathKitExt;
import io.jboot.utils.CacheUtil;
import io.jboot.utils.CollectionUtil;
import io.jboot.web.controller.annotation.RequestMapping;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RequestMapping("/")
public class SystemController extends BaseController {
	@Inject
	UserService userService;
	@Inject
	ResourceService resourceService;

	public void index() {
		redirect("/index.html");
	}

	@Clear(AuthInterceptor.class)
	public void captcha() {
		renderCaptcha();
	}

	@Clear(AuthInterceptor.class)
	public void login(String un, String pwd, boolean rememberMe) {
        if(!validateCaptcha("captcha")){
            renderJson(Ret.err(SysConstant.CAPTCHA_ERR));
        }else{
            renderJson(userService.login(un, pwd,rememberMe));
        }
		//renderJson(userService.login(un, pwd, rememberMe));
	}

	public void clearCache() {
		for (String cache : SysConstant.CACHES) {
			CacheUtil.removeAll(cache);
		}
		Ret ok = Ret.ok();
		ok.setMsg("清理成功");
		renderJson(ok);
	}

	public void getLoginUser() {
		renderJson(Ret.ok(userService.getById(StpUtil.getLoginIdAsInt())));
	}

	public void logout() {
		StpUtil.logout();
		redirect("/login.html");
	}

	public void listMenus() {
		User loginUser = userService.getById(StpUtil.getLoginIdAsInt());
		Menu homeInfo = resourceService.getMenu(loginUser.getRoleId(), Resource.TYPE_HOME);
		Menu logoInfo = resourceService.getMenu(loginUser.getRoleId(), Resource.TYPE_LOGO);
		List<Menu> menus = resourceService.listMenu(loginUser.getRoleId());
		renderJson(Kv.by("homeInfo", homeInfo)
				.set("logoInfo", logoInfo)
				.set("menuInfo", menus));
	}

	public void downLoad(String file) {
		Path path = Paths.get(PathKitExt.getWebRootPath(), file);
		renderFile(path.toFile());
	}

	/***
	 * 导入进度
	 */
	public void listImportProgress(Integer status) {
		List<String> keys = CacheUtil.getKeys(SysConstant.CACHE_IMPORT_PROGRESS);
		List<ImportProgress> progresses;
		if (CollectionUtil.isEmpty(keys)) {
			progresses = ListUtils.newArrayList();
		} else {
			progresses = ListUtils.newArrayListWithExpectedSize(keys.size());
			for (String key : keys) {
				ImportProgress progress = CacheUtil.get(SysConstant.CACHE_IMPORT_PROGRESS, key);
				if (progress == null) {
					continue;
				}
				if (status != null) {
					if (status.equals(progress.status.ordinal())) {
						progresses.add(progress);
					}
				} else {
					progresses.add(progress);
				}
			}
		}

		renderJson(Kv.by("code", 0).set("msg", "").set("count", progresses.size()).set("data", progresses));
	}

	public void delImportProgress(String no) {
		CacheUtil.remove(SysConstant.CACHE_IMPORT_PROGRESS, no);
		renderJson(Ret.ok());
	}

}
