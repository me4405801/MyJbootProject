package com.dage.salesflow.controller;

import com.dage.salesflow.kit.Ret;
import com.dage.salesflow.model.Area;
import com.dage.salesflow.service.AreaService;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import io.jboot.web.controller.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/area")
public class AreaController extends BaseController {
	@Inject
	AreaService areaService;

	public void list(String name) {
		List list = areaService.list(name);
		renderJson(Kv.by("code", 0).set("msg", "").set("count", list.size()).set("data", list));
	}

	public void listTree() {
		renderJson(Ret.ok(areaService.listTree(0)));
	}

	public void listByPage(String name, int page, int limit) {
		renderPageJson(areaService.listByPage(name, page, limit));
	}

	public void add() {
		renderJson(areaService.add(getBean(Area.class, "")));
	}

	public void edit() {
		renderJson(areaService.edit(getBean(Area.class, "")));
	}

	public void del() {
		renderJson(areaService.del(getBean(Area.class, "")));
	}

}
