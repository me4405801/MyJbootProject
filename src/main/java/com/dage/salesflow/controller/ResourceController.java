package com.dage.salesflow.controller;

import com.dage.salesflow.kit.Ret;
import com.dage.salesflow.model.Resource;
import com.dage.salesflow.service.ResourceService;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import io.jboot.web.controller.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/resource")
public class ResourceController extends BaseController {
	@Inject
	private ResourceService resourceService;

	/**
	 * 菜单列表
	 */
	public void list(String name, String code, Integer type) {
		List<Resource> list = resourceService.list(name, code, type);
		renderJson(Kv.by("code", 0).set("msg", "").set("count", list.size()).set("data", list));
	}

	public void listHome() {
		renderJson(Kv.by("code", 0).set("data", resourceService.listHome()));
	}

	public void add() {
		Resource resource = getBean(Resource.class, "");
		if (resourceService.save(resource) != null) {
			renderJson(Ret.ok());
		} else {
			renderJson(Ret.err("添加失败"));
		}
	}

	public void update() {
		Resource resource = getBean(Resource.class, "");
		if (resourceService.update(resource)) {
			renderJson(Ret.ok());
		} else {
			renderJson(Ret.err("修改失败"));
		}
	}

	public void del(int id) {
		renderJson(resourceService.del(id));
	}


}
