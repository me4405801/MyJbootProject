package com.dage.salesflow.controller;

import com.dage.salesflow.excel.model.ProductProfileExcel;
import com.dage.salesflow.model.ProductProfile;
import com.dage.salesflow.service.ProductProfileService;
import com.jfinal.aop.Inject;
import com.jfinal.core.paragetter.Para;
import io.jboot.web.controller.annotation.RequestMapping;

import java.io.File;
import java.io.IOException;

@RequestMapping("/productProfile")
public class ProductProfileController extends BaseController {
	@Inject
	ProductProfileService productProfileService;

	public void listByPage(@Para("") ProductProfile model, int page, int limit) {
		renderPageJson(productProfileService.listByPage(model, page, limit));
	}

	public void add(@Para("") ProductProfile productProfile) throws Exception {
		renderJson(productProfileService.add(productProfile));
	}

	public void edit(@Para("") ProductProfile productProfile) throws Exception {
		renderJson(productProfileService.edit(productProfile));
	}

	public void del(int id) {
		renderJson(productProfileService.del(id));
	}

	public File importTemplate() throws IOException {
		return importTemplateFile(ProductProfileExcel.class);
	}

	public File export(@Para("") ProductProfile model) throws Exception {
		return productProfileService.export(model);
	}

	public void importData(File file, boolean update) {
		renderJson(productProfileService.importData(file, update));
	}
}
