package com.dage.salesflow.controller;

import com.dage.salesflow.kit.Ret;
import com.dage.salesflow.model.Resource;
import com.dage.salesflow.model.Role;
import com.dage.salesflow.service.ResourceService;
import com.dage.salesflow.service.RoleService;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import io.jboot.web.controller.annotation.RequestMapping;

@RequestMapping("/role")
public class RoleController extends BaseController {
	@Inject
	RoleService roleService;
	@Inject
	ResourceService resourceService;

	public void listByPage(String roleName, int page, int limit) {
		renderPageJson(roleService.listByPage(roleName, page, limit));
	}

	public void listAll() {
		renderJson(Ret.ok(renderToString("/template/select-template.html", Kv.by("list", modelListToSelect(roleService.findAll())))));
	}

	public void listResource(int id, int type) {
		renderJson(Ret.ok(resourceService.listRoleResource(id, type)));
	}

	public void setHome(int id, int home) {
		if (roleService.setHome(id, home)) {
			renderJson(Ret.ok());
		} else {
			renderJson(Ret.err("设置失败"));
		}
	}

	public void add() {
		if (roleService.save(getBean(Role.class, "")) != null) {
			renderJson(Ret.ok());
		} else {
			renderJson(Ret.err("添加失败"));
		}
	}

	public void update() {
		if (roleService.update(getBean(Role.class, ""))) {
			renderJson(Ret.ok());
		} else {
			renderJson(Ret.err("修改失败"));
		}
	}

	public void del(int id) {
		renderJson(roleService.del(id));
	}

	public void saveMenu(int roleId) {
		Integer[] menus = getParaValuesToInt("ids[]");
		if (roleService.saveResource(roleId, Resource.TYPE_MENU, menus)) {
			renderJson(Ret.ok());
		} else {
			renderJson(Ret.err("设置失败"));
		}
	}

	public void saveResource(int roleId) {
		Integer[] resources = getParaValuesToInt("ids[]");
		if (roleService.saveResource(roleId, Resource.TYPE_PERMISSION, resources)) {
			renderJson(Ret.ok());
		} else {
			renderJson(Ret.err("设置失败"));
		}
	}


}
