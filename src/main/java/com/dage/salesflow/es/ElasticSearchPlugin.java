package com.dage.salesflow.es;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.shutdown.ElasticsearchShutdownClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.LogKit;
import com.jfinal.plugin.IPlugin;
import io.jboot.exception.JbootException;
import org.apache.http.HttpHost;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.RestClient;

import java.io.IOException;
import java.util.List;

public class ElasticSearchPlugin implements IPlugin {

	private String ip;
	private int port;
	private String clusterName;
	private static ElasticsearchClient elasticsearchClient;
	//	private static RestHighLevelClient restHighLevelClient;
	//	private static RequestOptions options = RequestOptions.DEFAULT;

	public ElasticSearchPlugin(String ip, int port, String clusterName) {
		this.ip = ip;
		this.port = port;
		this.clusterName = clusterName;
	}

	public static ElasticsearchClient getElasticsearchClient() {
		return elasticsearchClient;
	}

	@Override
	public boolean start() {
		RestClient restClient = RestClient.builder(new HttpHost(this.ip, this.port)).build();
		List<Node> nodes = restClient.getNodes();
		if (nodes.isEmpty()) {
			LogKit.info("未发现任何ES节点");
		} else {
			for (Node node : nodes) {
				LogKit.info("ES节点信息：" + JsonKit.toJson(node));
			}
		}
		LogKit.info("ElasticsearchClient 连接成功,节点数量：" + nodes.size());
		elasticsearchClient = new ElasticsearchClient(new RestClientTransport(restClient, new JacksonJsonpMapper()));
		//    开启下面初始化需要启动ES服务，否则报错
		//		try {
		//			CustomerProfileES.init();
		//		} catch (IOException e) {
		//			throw new JbootException(e);
		//		}
		return true;
	}

	@Override
	public boolean stop() {
		if (elasticsearchClient != null) {
			ElasticsearchShutdownClient client = elasticsearchClient.shutdown();
		}
		return true;
	}


	//	public static RestHighLevelClient getHighLevelClient(){
	//		return restHighLevelClient;
	//	}
	//
	//	public static RequestOptions getRequestOptions(){
	//		options = RequestOptions.DEFAULT;
	//		return options;
	//	}

	//	@Override
	//	public boolean start() {
	//		/**
	//		 * 这里的连接方式指的是没有安装x-pack插件,如果安装了x-pack则参考{@link ElasticsearchXPackClient}
	//		 * 1. java客户端的方式是以tcp协议在9300端口上进行通信
	//		 * 2. http客户端的方式是以http协议在9200端口上进行通信
	//		 */
	//		try {
	//			RestClientBuilder builder = RestClient.builder(new HttpHost(this.ip, this.port));
	////			restClient = builder.build();
	//			restHighLevelClient = new RestHighLevelClient(builder);
	//			RestClient restClient = restHighLevelClient.getLowLevelClient();
	//			List<Node> nodes = restClient.getNodes();
	//			if (nodes.isEmpty()) {
	//				LogKit.info("未发现任何ES节点");
	//			}else {
	//				for (Node node : nodes){
	//					LogKit.info("ES节点信息："+ JsonKit.toJson(node));
	//				}
	//			}
	//			LogKit.info("ElasticsearchClient 连接成功,节点数量："+nodes.size());
	//		} catch (Exception e) {
	////			LogKit.info(e.getMessage());
	//			throw e;
	//		}
	//		return true;
	//	}
	//
	//	@Override
	//	public boolean stop() {
	//		if (restHighLevelClient != null){
	//			try {
	//				restHighLevelClient.close();
	//			} catch (IOException e) {
	//				e.printStackTrace();
	//			}
	//		}
	//		return true;
	//	}
}
