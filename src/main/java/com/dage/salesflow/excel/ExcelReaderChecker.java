package com.dage.salesflow.excel;

import com.dage.salesflow.kit.Ret;
import io.jboot.db.model.JbootModel;

public interface ExcelReaderChecker<RECORD extends JbootModel<RECORD>> {
	Ret addCheck(RECORD model) throws Exception;

	Ret editCheck(RECORD model);
}
