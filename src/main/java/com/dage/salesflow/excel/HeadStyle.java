package com.dage.salesflow.excel;

import java.lang.annotation.*;

/**
 * Excel模型类表头样式
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface HeadStyle {
	/**
	 * 背景颜色（默认红）
	 */
	short value() default 10;

	/**
	 * 填充颜色（默认红）
	 */
	short fillColor() default 10;
}
