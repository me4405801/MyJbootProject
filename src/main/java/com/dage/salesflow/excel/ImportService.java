package com.dage.salesflow.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.dage.salesflow.kit.Ret;
import com.jfinal.kit.Kv;
import io.jboot.db.model.JbootModel;

import java.util.List;

public interface ImportService<RECORD extends JbootModel<RECORD>, EXCEL> {
	/**
	 * 添加时校验
	 */
	Ret addCheck(RECORD model) throws Exception;

	/**
	 * 修改时校验
	 */
	Ret editCheck(RECORD model) throws Exception;

	/**
	 * 由Excel模型对象设置数据库实体对象的属性，二者中相同的属性已自动完成设置
	 *
	 * @param importModel 导入数据对象
	 */
	Ret setModelData(ImportModel<EXCEL, RECORD> importModel, AnalysisContext context) throws Exception;

	/**
	 * 导入覆盖模式时 返回 数据id
	 *
	 * @param importModel 导入数据对象
	 */
	Integer queryId(ImportModel<EXCEL, RECORD> importModel) throws Exception;

	/**
	 * 导入覆盖模式时 设置 数据id
	 *
	 * @param record 数据实体类对象
	 * @param id
	 */
	void setModelId(RECORD record, Integer id);

	/**
	 * 操作数据库之前回调
	 *
	 * @param insertList 待插入数据
	 * @param updateList 待更新数据
	 */
	default void beforeDbBatch(List<RECORD> insertList, List<RECORD> updateList) {

	}

	/**
	 * 用于读取进度
	 *
	 * @param rowNum    总行数
	 * @param extraData 额外数据
	 * @param context
	 */
	default void onSheetStart(Integer rowNum, Kv extraData, AnalysisContext context) {
	}

	/**
	 * 用于读取进度
	 *
	 * @param importModel 导入数据对象（包含当前行号）
	 * @param context
	 */
	default void onSheetProgress(ImportModel<EXCEL, RECORD> importModel, AnalysisContext context) {
	}
}
