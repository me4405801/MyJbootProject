package com.dage.salesflow.excel;

import com.dage.salesflow.constant.ImportStatus;
import com.dage.salesflow.constant.ImportType;

import java.util.Date;

/**
 * 导入进度信息
 */
public class ImportProgress {
	/**
	 * 类型
	 */
	public ImportType type;
	/**
	 * 导入BatchNo
	 */
	public String no;
	/**
	 * 创建人
	 */
	public String creator;
	/**
	 * 创建时间
	 */
	public Date createTime;
	/**
	 * 文件名
	 */
	public String fileName;
	/**
	 * 总阶段数
	 */
	public Integer totalStep;
	/**
	 * 当前阶段
	 */
	public Integer currentStep = 1;
	/**
	 * 总行数
	 */
	public Integer totalNum;
	/**
	 * 当行数
	 */
	public Integer currentNum = 1;
	/**
	 * 当前状态
	 */
	public ImportStatus status;

	public ImportProgress(ImportType type, String no, String creator, Date createTime, String fileName, Integer totalStep, Integer totalNum, ImportStatus status) {
		this.type = type;
		this.no = no;
		this.creator = creator;
		this.createTime = createTime;
		this.fileName = fileName;
		this.totalStep = totalStep;
		this.totalNum = totalNum;
		this.status = status;
	}

	public ImportType getType() {
		return type;
	}

	public void setType(ImportType type) {
		this.type = type;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getTotalStep() {
		return totalStep;
	}

	public void setTotalStep(Integer totalStep) {
		this.totalStep = totalStep;
	}

	public Integer getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(Integer currentStep) {
		this.currentStep = currentStep;
	}

	public Integer getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}

	public Integer getCurrentNum() {
		return currentNum;
	}

	public void setCurrentNum(Integer currentNum) {
		this.currentNum = currentNum;
	}

	public ImportStatus getStatus() {
		return status;
	}

	public void setStatus(ImportStatus status) {
		this.status = status;
	}
}
