package com.dage.salesflow.excel;

import java.lang.annotation.*;

/**
 * Excel模型类表头备注
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface HeadNotation {
	/**
	 * 备注内容
	 */
	String value();

	/**
	 * 备注占用行数（默认1）
	 */
	int rows() default 1;

	/**
	 * 备注占用列数（默认1）
	 */
	int cols() default 1;
}
