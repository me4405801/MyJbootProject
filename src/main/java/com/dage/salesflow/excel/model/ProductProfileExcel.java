package com.dage.salesflow.excel.model;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.dage.salesflow.excel.HeadNotation;
import com.dage.salesflow.excel.HeadStyle;
import com.dage.salesflow.model.Dict;
import com.dage.salesflow.validation.*;

@ImportKey(columns = {"code"}, withDataScope = false, msg = "产品编码不能重复")
public class ProductProfileExcel {

	/**
	 * 产品编码
	 */
	@NotNull("产品编码未填写")
	@HeadStyle
	@HeadNotation("必填，不能重复")
	@ExcelProperty("产品编码")
	private String code;
	/**
	 * 产品名称
	 */
	@NotNull("产品名称未填写")
	@HeadStyle
	@HeadNotation("必填")
	@ExcelProperty("产品名称")
	private String name;
	@ExcelIgnore
	@DictKey(msg = "产品单位匹配失败", dictKey = Dict.PRODUCT_UNIT_KEY, notNull = false)
	private Integer unit;
	/**
	 * 产品单位（t_dict:product_unit）
	 */
	@ExcelProperty("产品单位")
	private String unitStr;
	@ExcelIgnore
	@DictKey(msg = "产品规格匹配失败", dictKey = Dict.PRODUCT_SPEC_KEY)
	private Integer spec;
	/**
	 * 产品规格（t_dict:product_spec）
	 */
	@HeadStyle
	@HeadNotation("必填")
	@ExcelProperty("产品规格")
	private String specStr;
	@ExcelIgnore
	@DictKey(msg = "产品类别匹配失败", dictKey = Dict.PRODUCT_TYPE_KEY, notNull = false)
	private Integer type;
	/**
	 * 产品类别（t_dict:product_type）
	 */
	@ExcelProperty("产品类别")
	private String typeStr;
	/**
	 * 件装量
	 */
	@ExcelProperty("件装量")
	private Integer capacity;
	/**
	 * 有效期（月）
	 */
	@ExcelProperty("有效期（月）")
	private Integer expiryDate;
	@ExcelIgnore
	@EnumProp(yesNo = true, msg = "是否医保匹配失败")
	private Integer isMedical;
	/**
	 * 是否医保0.否1.是
	 */
	@ExcelProperty("是否医保")
	private String isMedicalName;
	@ExcelIgnore
	@EnumProp(yesNo = true, msg = "是否基药匹配失败")
	private Integer isBasicDrug;
	/**
	 * 是否基药0.否1.是
	 */
	@ExcelProperty("是否基药")
	private String isBasicDrugName;
	@ExcelIgnore
	@ForeignKey(msg = "区域匹配失败", table = "t_area", column = "name", withDataScope = false, logicDelete = false, notNull = false)
	private Integer areaId;
	/**
	 * 区域id(t_area.id)
	 */
	@ExcelProperty("区域")
	private String areaIdStr;

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnitStr(String unitStr) {
		this.unitStr = unitStr;
	}

	public String getUnitStr() {
		return unitStr;
	}

	public void setSpec(Integer spec) {
		this.spec = spec;
	}

	public Integer getSpec() {
		return spec;
	}

	public void setSpecStr(String specStr) {
		this.specStr = specStr;
	}

	public String getSpecStr() {
		return specStr;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getType() {
		return type;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	public String getTypeStr() {
		return typeStr;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setExpiryDate(Integer expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getExpiryDate() {
		return expiryDate;
	}

	public void setIsMedical(Integer isMedical) {
		this.isMedical = isMedical;
	}

	public Integer getIsMedical() {
		return isMedical;
	}

	public void setIsMedicalName(String isMedicalName) {
		this.isMedicalName = isMedicalName;
	}

	public String getIsMedicalName() {
		return isMedicalName;
	}

	public void setIsBasicDrug(Integer isBasicDrug) {
		this.isBasicDrug = isBasicDrug;
	}

	public Integer getIsBasicDrug() {
		return isBasicDrug;
	}

	public void setIsBasicDrugName(String isBasicDrugName) {
		this.isBasicDrugName = isBasicDrugName;
	}

	public String getIsBasicDrugName() {
		return isBasicDrugName;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaIdStr(String areaIdStr) {
		this.areaIdStr = areaIdStr;
	}

	public String getAreaIdStr() {
		return areaIdStr;
	}

}

