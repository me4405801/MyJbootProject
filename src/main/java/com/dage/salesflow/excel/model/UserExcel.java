package com.dage.salesflow.excel.model;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.dage.salesflow.excel.HeadNotation;
import com.dage.salesflow.excel.HeadStyle;
import com.dage.salesflow.validation.*;

@ImportKey(columns = {"name"}, logicDelete = false, withDataScope = false)
public class UserExcel {

	/**
	 * 用户名
	 */
	@ExcelProperty("用户名")
	@ExcelIgnore
	@UniqueKey(value = "用户名不能重复", logicDelete = false, withDataScope = false)
	private String username;
	/**
	 * 密码
	 */
	@ExcelProperty("密码")
	@ExcelIgnore
	private String password;
	/**
	 * 用户姓名
	 */
	@NotNull("用户姓名未填写")
	@HeadStyle
	@HeadNotation("必填，不能重复")
	@ExcelProperty("用户姓名")
	@UniqueKey(value = "用户姓名不能重复", logicDelete = false, withDataScope = false)
	private String name;
	@ExcelIgnore
	@ForeignKey(msg = "角色（岗位）匹配失败", table = "t_role", column = "role_name", withDataScope = false, logicDelete = false)
	private Integer roleId;
	/**
	 * 角色（岗位）
	 */
	@HeadStyle
	@HeadNotation("必填")
	@ExcelProperty("角色（岗位）")
	private String roleIdStr;
	@ExcelIgnore
	@ForeignKey(msg = "区域匹配失败", table = "t_area", column = "name", withDataScope = false, logicDelete = false)
	private Integer areaId;
	/**
	 * 区域
	 */
	@HeadStyle
	@HeadNotation("必填")
	@ExcelProperty("区域")
	private String areaIdStr;
	/**
	 * 员工编号
	 */
	@NotNull("员工编号未填写")
	@HeadStyle
	@HeadNotation("必填，不能重复")
	@ExcelProperty("员工编号")
	@UniqueKey(value = "员工编号不能重复", logicDelete = false, withDataScope = false)
	private String code;
	@ExcelIgnore
	@EnumProp(props = {"女", "男"}, msg = "性别匹配失败")
	private Integer sex;
	/**
	 * 性别：1.男0.女
	 */
	@ExcelProperty("性别")
	private String sexName;
	/**
	 * 微信
	 */
	@ExcelProperty("微信")
	private String weixin;
	/**
	 * 手机号
	 */
	@ExcelProperty("手机号")
	@RegexValidation(msg = "手机号格式错误", pattern = ValidationInfo.REG_MOBILE)
	private String phone;
	/**
	 * 电子邮件
	 */
	@ExcelProperty("电子邮件")
	@RegexValidation(msg = "电子邮件格式错误", pattern = ValidationInfo.REG_EMAIL)
	private String email;
	/**
	 * QQ
	 */
	@ExcelProperty("QQ")
	private String qq;
	/**
	 * 身份证
	 */
	@ExcelProperty("身份证")
	@RegexValidation(msg = "身份证格式错误", pattern = ValidationInfo.REG_ID_CARD)
	private String idNumber;
	@ExcelIgnore
	@EnumProp(props = {"封停", "正常"}, msg = "账户状态匹配失败")
	private Integer status;
	/**
	 * 账户状态：0.封停1.正常
	 */
	@ExcelProperty("账户状态")
	private String statusName;
	@ExcelIgnore
	private java.util.Date entryTime;
	/**
	 * 入职时间
	 */
	@HeadNotation(" 格式 年-月-日")
	@ExcelProperty("入职时间")
	private String entryTimeStr;
	/**
	 * 备注
	 */
	@ExcelProperty("备注")
	private String remark;

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleIdStr(String roleIdStr) {
		this.roleIdStr = roleIdStr;
	}

	public String getRoleIdStr() {
		return roleIdStr;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaIdStr(String areaIdStr) {
		this.areaIdStr = areaIdStr;
	}

	public String getAreaIdStr() {
		return areaIdStr;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSexName(String sexName) {
		this.sexName = sexName;
	}

	public String getSexName() {
		return sexName;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}

	public String getWeixin() {
		return weixin;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getQq() {
		return qq;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}

	public void setEntryTime(java.util.Date entryTime) {
		this.entryTime = entryTime;
	}

	public java.util.Date getEntryTime() {
		return entryTime;
	}

	public void setEntryTimeStr(String entryTimeStr) {
		this.entryTimeStr = entryTimeStr;
	}

	public String getEntryTimeStr() {
		return entryTimeStr;
	}

}

