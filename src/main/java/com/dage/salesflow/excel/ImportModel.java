package com.dage.salesflow.excel;

import com.dage.salesflow.kit.Ret;

/**
 * Excel导入模型类
 */
public class ImportModel<EXCEL, RECORD> {
	/**
	 * 当前行号
	 */
	public Integer rowIndex;
	/**
	 * 读取的excel数据
	 */
	public EXCEL excel;
	/**
	 * 由excel复制的数据库实体类对象
	 */
	public RECORD record;

	public Ret ret;

	public ImportModel(Integer rowIndex, EXCEL excel, RECORD record, Ret ret) {
		this.rowIndex = rowIndex;
		this.excel = excel;
		this.record = record;
		this.ret = ret;
	}
}
