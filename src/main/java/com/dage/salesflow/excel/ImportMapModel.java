package com.dage.salesflow.excel;

import com.dage.salesflow.kit.Ret;

import java.util.Map;

/**
 * Excel导入模型类，无对象导入
 */
public class ImportMapModel<EXCEL, RECORD> extends ImportModel<EXCEL, RECORD> {
	/**
	 * 读取的excel数据
	 */
	public Map<String, String> excelMap;

	public ImportMapModel(Integer rowIndex, Map<String, String> excelMap, RECORD record, Ret ret) {
		super(rowIndex, null, record, ret);
		this.excelMap = excelMap;
	}
}
