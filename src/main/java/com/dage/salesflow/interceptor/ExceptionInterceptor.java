package com.dage.salesflow.interceptor;

import cn.dev33.satoken.context.SaTokenContextForThreadLocalStorage;
import cn.dev33.satoken.servlet.model.SaRequestForServlet;
import cn.dev33.satoken.servlet.model.SaResponseForServlet;
import cn.dev33.satoken.servlet.model.SaStorageForServlet;
import com.dage.salesflow.kit.Ret;
import com.dage.salesflow.kit.WebKit;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

public class ExceptionInterceptor implements Interceptor {
	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		try {
			SaTokenContextForThreadLocalStorage.setBox(new SaRequestForServlet(controller.getRequest()), new SaResponseForServlet(controller.getResponse()), new SaStorageForServlet(controller.getRequest()));
			inv.invoke();
		} catch (Exception e) {
			e.printStackTrace();
			if (WebKit.isAjaxRequest(controller.getRequest())) {
				controller.renderJson(Ret.err(e.getMessage()));
			} else {
				controller.redirect("/err/500.html");
			}
		}
	}
}
