package com.dage.salesflow.model;

import com.dage.salesflow.model.vo.SelectOption;
import com.dage.salesflow.model.base.BaseRole;
import io.jboot.db.annotation.Table;

/**
 * Generated by Jboot.
 */
@Table(tableName = "t_role", primaryKey = "id")
public class Role extends BaseRole<Role> implements SelectOption {

	@Override
	public String getValue() {
		return String.valueOf(getId());
	}

	@Override
	public String getText() {
		return getRoleName();
	}
}
