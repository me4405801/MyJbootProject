package com.dage.salesflow.model.vo;

import java.util.List;

/**
 * 用于返回layui树形控件数据
 */
public class TreeNode {
	/**
	 * 节点唯一索引值，用于对指定节点进行各类操作
	 */
	private Integer id;
	/**
	 * 节点标题
	 */
	private String title;
	/**
	 * 节点字段名,一般对应表字段名
	 */
	private String field;
	/**
	 * 点击节点弹出新窗口对应的 url。需开启 isJump 参数
	 */
	private String href;

	private String code;
	/**
	 * 节点是否初始展开，默认 false
	 */
	private boolean spread;
	/**
	 * 节点是否初始为选中状态（如果开启复选框的话），默认 false
	 */
	private boolean checked;
	/**
	 * 节点是否为禁用状态。默认 false
	 */
	private boolean disabled;

	private List<TreeNode> children;

	public TreeNode() {
	}

	public TreeNode(String code, Integer id, String title) {
		this.id = id;
		this.title = title;
		this.code = code;
	}

	public TreeNode(Integer id, String title, String field) {
		this.id = id;
		this.title = title;
		this.field = field;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isSpread() {
		return spread;
	}

	public void setSpread(boolean spread) {
		this.spread = spread;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
}
