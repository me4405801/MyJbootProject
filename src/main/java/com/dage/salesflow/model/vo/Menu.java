package com.dage.salesflow.model.vo;

import java.util.List;

/**
 * web前台菜单项
 */
public class Menu {
	private Integer id;
	private String title;
	private String href;
	private String image;
	private String icon;
	private String target = "_self";
	private List<Menu> child;

	public Menu() {
	}

	public Menu(String title, String href, String icon) {
		this.title = title;
		this.href = href;
		this.icon = icon;
	}

	public Menu(Integer id, String title, String href, String icon) {
		this.id = id;
		this.title = title;
		this.href = href;
		this.icon = icon;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public List<Menu> getChild() {
		return child;
	}

	public void setChild(List<Menu> child) {
		this.child = child;
	}
}
