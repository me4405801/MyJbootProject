package com.dage.salesflow.model.vo;

public interface SelectOption {
	String getValue();

	String getText();
}
