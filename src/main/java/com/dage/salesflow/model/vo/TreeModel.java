package com.dage.salesflow.model.vo;

import java.util.List;

public interface TreeModel<M extends TreeModel> {
	Integer getId();

	Integer getParentId();

	M findById(Object id);

	M newById(Integer id);

	default List<M> listChildren(Integer pid) {
		return null;
	}
}
