package com.dage.salesflow.model.vo;

import java.util.List;

/**
 * 用于构件查询语句和参数
 */
public class QuerySql {
	public String selectSql;
	public String sqlExceptSelect;
	public List<Object> paras;

	public QuerySql(String selectSql, String sqlExceptSelect, List<Object> paras) {
		this.selectSql = selectSql;
		this.sqlExceptSelect = sqlExceptSelect;
		this.paras = paras;
	}
}
