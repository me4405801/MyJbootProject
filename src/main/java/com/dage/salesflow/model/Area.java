package com.dage.salesflow.model;

import com.dage.salesflow.model.base.BaseArea;
import com.dage.salesflow.model.vo.TreeModel;
import io.jboot.db.annotation.Table;

import java.util.List;

/**
 * Generated by Jboot.
 */
@Table(tableName = "t_area", primaryKey = "id")
public class Area extends BaseArea<Area> implements TreeModel<Area> {
	public static final int CODE_LEN = 5;

	@Override
	public Integer getParentId() {
		return getPid();
	}

	@Override
	public Area newById(Integer id) {
		Area area = new Area();
		area.setId(id);
		return area;
	}

	@Override
	public List<Area> listChildren(Integer pid) {
		return find("SELECT id,pid,name,code FROM `t_area` WHERE pid=?", pid);
	}
}

