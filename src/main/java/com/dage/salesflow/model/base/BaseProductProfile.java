package com.dage.salesflow.model.base;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseProductProfile<M extends BaseProductProfile<M>> extends BaseModel<M> {

	/**
	 * 产品编码
	 */
	public void setCode(String code) {
		set("code", code);
	}

	/**
	 * 产品编码
	 */
	public String getCode() {
		return getStr("code");
	}

	/**
	 * 产品名称
	 */
	public void setName(String name) {
		set("name", name);
	}

	/**
	 * 产品名称
	 */
	public String getName() {
		return getStr("name");
	}

	/**
	 * 产品单位（t_dict:product_unit）
	 */
	public void setUnit(Integer unit) {
		set("unit", unit);
	}

	/**
	 * 产品单位（t_dict:product_unit）
	 */
	public Integer getUnit() {
		return getInt("unit");
	}

	/**
	 * 产品规格（t_dict:product_spec）
	 */
	public void setSpec(Integer spec) {
		set("spec", spec);
	}

	/**
	 * 产品规格（t_dict:product_spec）
	 */
	public Integer getSpec() {
		return getInt("spec");
	}

	/**
	 * 产品类别（t_dict:product_type）
	 */
	public void setType(Integer type) {
		set("type", type);
	}

	/**
	 * 产品类别（t_dict:product_type）
	 */
	public Integer getType() {
		return getInt("type");
	}

	/**
	 * 件装量
	 */
	public void setCapacity(Integer capacity) {
		set("capacity", capacity);
	}

	/**
	 * 件装量
	 */
	public Integer getCapacity() {
		return getInt("capacity");
	}

	/**
	 * 有效期（月）
	 */
	public void setExpiryDate(Integer expiryDate) {
		set("expiry_date", expiryDate);
	}

	/**
	 * 有效期（月）
	 */
	public Integer getExpiryDate() {
		return getInt("expiry_date");
	}

	/**
	 * 是否医保0.否1.是
	 */
	public void setIsMedical(Integer isMedical) {
		set("is_medical", isMedical);
	}

	/**
	 * 是否医保0.否1.是
	 */
	public Integer getIsMedical() {
		return getInt("is_medical");
	}

	/**
	 * 是否基药0.否1.是
	 */
	public void setIsBasicDrug(Integer isBasicDrug) {
		set("is_basic_drug", isBasicDrug);
	}

	/**
	 * 是否基药0.否1.是
	 */
	public Integer getIsBasicDrug() {
		return getInt("is_basic_drug");
	}

	/**
	 * 区域id(t_area.id)
	 */
	public void setAreaId(Integer areaId) {
		set("area_id", areaId);
	}

	/**
	 * 区域id(t_area.id)
	 */
	public Integer getAreaId() {
		return getInt("area_id");
	}

}

