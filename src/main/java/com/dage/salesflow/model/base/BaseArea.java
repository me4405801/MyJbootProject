package com.dage.salesflow.model.base;

import io.jboot.db.model.JbootModel;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseArea<M extends BaseArea<M>> extends JbootModel<M> implements IBean {

	public void setId(Integer id) {
		set("id", id);
	}

	public Integer getId() {
		return getInt("id");
	}

	/**
	 * 上级区域id
	 */
	public void setPid(Integer pid) {
		set("pid", pid);
	}

	/**
	 * 上级区域id
	 */
	public Integer getPid() {
		return getInt("pid");
	}

	/**
	 * 区域名称
	 */
	public void setName(String name) {
		set("name", name);
	}

	/**
	 * 区域名称
	 */
	public String getName() {
		return getStr("name");
	}

	/**
	 * 区域编码
	 */
	public void setCode(String code) {
		set("code", code);
	}

	/**
	 * 区域编码
	 */
	public String getCode() {
		return getStr("code");
	}

	/**
	 * 排序序号
	 */
	public void setSort(Integer sort) {
		set("sort", sort);
	}

	/**
	 * 排序序号
	 */
	public Integer getSort() {
		return getInt("sort");
	}

	/**
	 * 备注
	 */
	public void setNote(String note) {
		set("note", note);
	}

	/**
	 * 备注
	 */
	public String getNote() {
		return getStr("note");
	}

}

