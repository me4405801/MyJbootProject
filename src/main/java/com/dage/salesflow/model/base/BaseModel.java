package com.dage.salesflow.model.base;

import com.dage.salesflow.json.IgnoreFields;
import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.model.JbootModel;

@IgnoreFields({"logicDelete", "withDataScope"})
public abstract class BaseModel<M extends JbootModel<M>> extends JbootModel<M> implements IBean {

	public boolean logicDelete = true;

	public boolean withDataScope = true;

	public void setId(Integer id) {
		set("id", id);
	}

	public Integer getId() {
		return getInt("id");
	}

	/**
	 * 创建人
	 */
	public void setCreateUser(java.lang.Integer createUser) {
		set("create_user", createUser);
	}

	/**
	 * 创建人
	 */
	public java.lang.Integer getCreateUser() {
		return getInt("create_user");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}

	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return getDate("create_time");
	}

	/**
	 * 修改人
	 */
	public void setUpdateUser(java.lang.Integer updateUser) {
		set("update_user", updateUser);
	}

	/**
	 * 修改人
	 */
	public java.lang.Integer getUpdateUser() {
		return getInt("update_user");
	}

	/**
	 * 修改时间
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}

	/**
	 * 修改时间
	 */
	public java.util.Date getUpdateTime() {
		return getDate("update_time");
	}

	/**
	 * 是否删除：0.否1.是
	 */
	public void setIsDeleted(java.lang.Integer isDeleted) {
		set("is_deleted", isDeleted);
	}

	/**
	 * 是否删除：0.否1.是
	 */
	public java.lang.Integer getIsDeleted() {
		return getInt("is_deleted");
	}
}
