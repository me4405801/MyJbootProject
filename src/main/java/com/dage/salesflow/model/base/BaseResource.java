package com.dage.salesflow.model.base;

import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.model.JbootModel;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseResource<M extends BaseResource<M>> extends JbootModel<M> implements IBean {

	public void setId(Integer id) {
		set("id", id);
	}

	public Integer getId() {
		return getInt("id");
	}

	public void setName(String name) {
		set("name", name);
	}

	public String getName() {
		return getStr("name");
	}

	/**
	 * 类型：0.菜单1.权限2.首页
	 */
	public void setType(Integer type) {
		set("type", type);
	}

	/**
	 * 类型：0.菜单1.权限2.首页
	 */
	public Integer getType() {
		return getInt("type");
	}

	/**
	 * 父菜单id
	 */
	public void setPid(Integer pid) {
		set("pid", pid);
	}

	/**
	 * 父菜单id
	 */
	public Integer getPid() {
		return getInt("pid");
	}

	public void setIcon(String icon) {
		set("icon", icon);
	}

	public String getIcon() {
		return getStr("icon");
	}

	/**
	 * 菜单url
	 */
	public void setUrl(String url) {
		set("url", url);
	}

	/**
	 * 菜单url
	 */
	public String getUrl() {
		return getStr("url");
	}

	public void setMenuOrder(Integer menuOrder) {
		set("menu_order", menuOrder);
	}

	public Integer getMenuOrder() {
		return getInt("menu_order");
	}

	/**
	 * 权限code
	 */
	public void setCode(String code) {
		set("code", code);
	}

	/**
	 * 权限code
	 */
	public String getCode() {
		return getStr("code");
	}

	public void setRemark(String remark) {
		set("remark", remark);
	}

	public String getRemark() {
		return getStr("remark");
	}

}

