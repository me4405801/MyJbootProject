package com.dage.salesflow.validation;

import java.lang.annotation.*;

/**
 * 自定义验证框架，系统字典字段，导入时使用
 * 导入时自动获取《属性名+Str》的属性值进行查询
 * 同时字典属性必须添加@ExcelIgnore
 * 例如：
 *
 * @ExcelIgnore
 * @DictKey(msg = "产品规格匹配失败", dictKey = Dict.PRODUCT_SPEC_KEY)
 * private Integer spec;
 * @ExcelProperty("产品规格") private String specStr;
 * <p>
 * 导入时 自动查询
 * SELECT id FROM t_dict WHERE dic_key='product_spec' AND dic_value=#{specStr}
 * <p>
 * 并且将查询的id设置到spec属性中
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface DictKey {
	/**
	 * 错误信息
	 */
	String msg();

	/**
	 * 关联字典表的dict_key
	 */
	String dictKey();

	/**
	 * 是否非空，默认 是
	 */
	boolean notNull() default true;

}
