package com.dage.salesflow.validation;

import com.alibaba.excel.util.ListUtils;
import com.alibaba.excel.util.MapUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * 验证信息
 */
public class ValidationInfo {
	/**
	 * 数据表名称
	 */
	public String table;
	/**
	 * 数据表备注名称
	 */
	public String name;
	/**
	 * 唯一性验证
	 */
	public final Map<Field, UniqueKeyInfo> uniqueKeyMap = MapUtils.newHashMap();
	/**
	 * 非空验证
	 */
	public final Map<Field, NotNullInfo> notNullMap = MapUtils.newHashMap();
	/**
	 * 外键关联
	 */
	public final Map<Field, ForeignKeyInfo> foreignKeyMap = MapUtils.newHashMap();
	/**
	 * 字典字段
	 */
	public final Map<Field, DictKeyInfo> dictKeyMap = MapUtils.newHashMap();
	/**
	 * 正则验证
	 */
	public final Map<Field, RegexValidationInfo> regExMap = MapUtils.newHashMap();
	/**
	 * 枚举属性
	 */
	public final Map<Field, EnumPropInfo> enumPropMap = MapUtils.newHashMap();

	public static final String REG_ID_CARD = "^(\\d{18}|\\d{15}|\\d{17}x)$";
	public static final String REG_POST_CODE = "^[1-9]\\d{5}$";
	public static final String REG_MOBILE = "(^(\\d{3,4}[-_－—])?\\d{7,8})$|(13[0-9]{9})";
	public static final String REG_FAX = "^(\\d{3,4}-)?\\d{7,8}$";
	public static final String REG_TELEPHONE = "^(\\+\\d{2}[-_－—])?0\\d{2,3}[-_－—]\\d{7,8}$";
	public static final String REG_TEL_MOBILE = "(^(\\+\\d{2}[-_－—])?0\\d{2,3}[-_－—]\\d{7,8}$)|(^(\\d{3,4}[-_－—])?\\d{7,8})$|(13[0-9]{9})";
	public static final String REG_EMAIL = "^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$";

	public ImportKeyInfo importKey = new ImportKeyInfo();

	public static class ImportKeyInfo {
		public String msg;
		public boolean withDataScope;
		public boolean logicDelete;
		public List<FieldInfo> fieldInfos = ListUtils.newArrayList();
	}

	public static class UniqueKeyInfo {
		public String msg;
		public boolean withDataScope;
		public boolean logicDelete;
		public FieldInfo fieldInfo = new FieldInfo();
	}

	public static class NotNullInfo {
		public String defaultValue;
		public String msg;
		public Method recordGetter;
	}

	public static class ForeignKeyInfo {
		public String msg;
		public String table;
		public String sql;
		public boolean withDataScope;
		public boolean logicDelete;
		public boolean notNull;
		public FieldInfo fieldInfo = new FieldInfo();
	}

	public static class DictKeyInfo {
		public String msg;
		public String dictKey;
		public boolean notNull;
		public FieldInfo fieldInfo = new FieldInfo();
	}

	public static class RegexValidationInfo {
		public String msg;
		public String pattern;
		public Method recordSetter;
	}

	public static class EnumPropInfo {
		public String msg;
		public String[] props;
		public boolean yesNo;
		public FieldInfo fieldInfo = new FieldInfo();
	}

	public static class FieldInfo {
		public String column;
		public Field excelField;
		public Method recordSetter;
		public Method recordGetter;
	}
}
