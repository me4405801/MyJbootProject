package com.dage.salesflow.validation;

import java.lang.annotation.*;

/**
 * 自定义验证框架，正则验证
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface RegexValidation {
	/**
	 * 错误信息
	 */
	String msg() default "格式错误";

	/**
	 * 验证规则名称，详见ValidationInfo
	 */
	String pattern();
}
