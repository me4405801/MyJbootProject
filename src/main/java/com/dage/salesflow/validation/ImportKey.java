package com.dage.salesflow.validation;

import java.lang.annotation.*;

/**
 * 自定义验证框架，唯一键约束，导入、新增、修改时重复性校验，导入覆盖模式唯一性查询
 * 如果不需要覆盖模式导入，或者有其他特殊需求，请删除本注解，并覆写方法BaseService#queryId自行实现
 * 例如：
 *
 * @ImportKey(columns = {"product_id","customer_id","year_num"},withDataScope = false)
 * public class InventoryConfigExcel {
 * }
 * <p>
 * 假设当前登录用户对应的区域code为00001
 * <p>
 * 以覆盖模式导入时候，自动查询（可以重新覆写方法BaseService#queryId）
 * SELECT * FROM inventory_config WHERE product_id=? AND customer_id=? AND year_num=? AND is_deleted=0
 * if(id!=null({
 * 以此id做更新操作
 * }else{
 * 插入操作
 * }
 * <p>
 * 插入时，自动查询：（可以重新覆写方法BaseService#uniqueCheck或BaseService#addCheck）
 * SELECT COUNT(*) FROM inventory_config WHERE product_id=? AND customer_id=? AND year_num=? AND is_deleted=0
 * 如果count>0则插入失败
 * <p>
 * 更新时，自动查询：（可以重新覆写方法BaseService#uniqueCheck或BaseService#editCheck）
 * SELECT COUNT(*) FROM inventory_config WHERE product_id=? AND customer_id=? AND year_num=? AND is_deleted=0 AND id!=?
 * 如果count>0则更新失败
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ImportKey {
	/**
	 * 错误信息
	 */
	String msg() default "数据重复";

	/**
	 * 数据表字段名称，可以多个
	 */
	String[] columns();

	/**
	 * 重复性校验是否应用数据权限，默认 是
	 */
	boolean withDataScope() default true;

	/**
	 * 是否逻辑删除，默认 是
	 */
	boolean logicDelete() default true;
}
