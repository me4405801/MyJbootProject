package com.dage.salesflow.validation;

import java.lang.annotation.*;

/**
 * 自定义验证框架，外键关联约束，导入时使用
 * 导入时自动获取《属性名+Str》的属性值进行查询
 * 同时外键属性必须添加@ExcelIgnore
 * 例如：
 *
 * @ExcelIgnore
 * @ForeignKey(msg = "角色（岗位）匹配失败",table = "t_role",column = "role_name", withDataScope = false, logicDelete = false)
 * private Integer roleId;
 * @ExcelProperty("角色（岗位）") private String roleIdStr;
 * <p>
 * 导入时 自动查询
 * SELECT id FROM t_role WHERE name=#{roleIdStr}
 * 并且将查询的id设置到roleId属性中
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ForeignKey {
	/**
	 * 错误信息
	 */
	String msg();

	/**
	 * 关联数据表字段名称，导入时依此字段查询table表的id
	 */
	String column();

	/**
	 * 关联数据表名称
	 */
	String table();

	/**
	 * 额外的查询条件
	 */
	String sql() default "";

	/**
	 * 是否非空，默认 是
	 */
	boolean notNull() default true;

	/**
	 * 是否应用数据权限，默认 是
	 */
	boolean withDataScope() default true;

	/**
	 * 是否逻辑删除，默认 是
	 */
	boolean logicDelete() default true;

}
