package com.dage.salesflow.validation;

import java.lang.annotation.*;

/**
 * 自定义验证框架，枚举属性
 * 针对固定的枚举属性，类型必须为Integer，如：is_deleted 0.未删除1.已删除
 * 同时枚举属性必须添加@ExcelIgnore
 * 导入、导出时自动查询《属性名+Name》的属性进行操作
 * 例如：
 *
 * @ExcelIgnore
 * @EnumProp(props ={"离职","在职"}, msg = "任职状态匹配失败")
 * private Integer workStatus;
 * @ExcelProperty("任职状态") private String workStatusName;
 * <p>
 * 导入时：
 * 假设workStatusName="在职"
 * 则自动设置Model对象的workStatus属性值为1
 * 导出时：
 * 假设workStatus= 0
 * 则自动设置Excel对象的workStatusName属性值为"离职"
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface EnumProp {
	/**
	 * 枚举数组
	 */
	String[] props() default {"否", "是"};

	/**
	 * 错误信息
	 */
	String msg() default "匹配失败";

	/**
	 * “是否”类型属性，默认不是（false），如果设置为true，则忽略props设置，自动取SysConstant.YES_NO_PROP
	 */
	boolean yesNo() default false;
}
