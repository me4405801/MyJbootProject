package com.dage.salesflow.validation;

/**
 * 保存删除校验信息
 */
public class DelForeignInfo {
	/**
	 * 用于删除校验查询关联数量的sql，只接受一个int类型的id参数
	 */
	public String sql;
	/**
	 * 关联表名称
	 */
	public String name;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		DelForeignInfo that = (DelForeignInfo) o;

		return sql.equals(that.sql);
	}

	@Override
	public int hashCode() {
		return sql.hashCode();
	}
}
