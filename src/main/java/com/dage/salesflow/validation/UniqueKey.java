package com.dage.salesflow.validation;

import java.lang.annotation.*;

/**
 * 自定义验证框架，唯一键约束，导入、新增、修改时重复性校验
 * 例如：
 * <p>
 * public class EmployeeExcel {
 *
 * @ExcelProperty("工号")
 * @UniqueKey(value = "工号不能重复")
 * @NotNull("员工工号未填写") private String code;
 * }
 * <p>
 * 假设当前登录用户对应的区域code为00001
 * <p>
 * 插入时，自动查询：（可以重新覆写方法BaseService#uniqueCheck或BaseService#addCheck）
 * SELECT COUNT(*) FROM `employee_profile` WHERE area_id IN(SELECT id FROM t_area WHERE `code` LIKE '00001%') AND is_deleted=0 AND `code`=#{code}
 * 如果count>0则插入失败
 * <p>
 * 更新时，自动查询：（可以重新覆写方法BaseService#uniqueCheck或BaseService#editCheck）
 * SELECT COUNT(*) FROM `employee_profile` WHERE area_id IN(SELECT id FROM t_area WHERE `code` LIKE '00001%') AND is_deleted=0 AND `code`=#{code} AND id!=#{id}
 * 如果count>0则更新失败
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UniqueKey {
	/**
	 * 错误信息
	 */
	String value() default "字段不能重复";

	/**
	 * 数据表字段名称，默认和属性名相同
	 */
	String column() default "";

	/**
	 * 重复性校验是否应用数据权限，默认 是
	 */
	boolean withDataScope() default true;

	/**
	 * 是否逻辑删除，默认 是
	 */
	boolean logicDelete() default true;
}
