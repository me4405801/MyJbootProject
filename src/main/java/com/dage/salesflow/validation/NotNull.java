package com.dage.salesflow.validation;

import java.lang.annotation.*;

/**
 * 自定义验证框架，非空约束
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface NotNull {
	/**
	 * 错误信息
	 */
	String value() default "非空字段未填写";

	/**
	 * 默认值
	 */
	String defaultValue() default "";
}
