package com.dage.salesflow.validation;

import java.lang.annotation.*;

/**
 * 映射数据实体类对应的Excel模型类
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ExcelModel {
	/**
	 * Excel模型类
	 */
	Class excelClass();

	/**
	 * 备注表名
	 */
	String name() default "";
}
