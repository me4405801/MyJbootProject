package com.dage.salesflow.shardingjdbc;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;

public class PeriodPreciseShardingAlgorithm implements PreciseShardingAlgorithm<Integer> {
	@Override
	public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Integer> shardingValue) {
		return shardingValue.getLogicTableName() + shardingValue.getValue();
	}
}
