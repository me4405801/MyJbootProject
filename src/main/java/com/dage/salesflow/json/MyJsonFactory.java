package com.dage.salesflow.json;

import io.jboot.web.json.JbootJson;

import java.util.Map;

/**
 * 自定义json转换，配合自定义注解IgnoreFields使用
 */
public class MyJsonFactory extends JbootJson {

	@Override
	protected void fillBeanToMap(Object bean, Map toMap) {
		super.fillBeanToMap(bean, toMap);
		Class<?> clazz = bean.getClass();
		IgnoreFields ignoreFields = clazz.getAnnotation(IgnoreFields.class);
		if (ignoreFields != null) {
			String[] fields = ignoreFields.value();
			for (String field : fields) {
				toMap.remove(field);
			}
		}
	}
}
