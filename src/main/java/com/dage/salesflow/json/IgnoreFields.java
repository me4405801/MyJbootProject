package com.dage.salesflow.json;

import java.lang.annotation.*;

/**
 * 自定义json注解，只能在类上声明，填写需要忽略的字段名称（多个以逗号分割为数组）
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface IgnoreFields {
	String[] value();
}
